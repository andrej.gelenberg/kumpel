# Description

Kumpel is a C library for organizing an batch processing queue for independent tasks,
which is then processed by 1 or multiple worker threads.

# Install

Checkout the source code from here:  https://bitbucket.org/nikel123/kumpel

Then run in command line:

  $ make && sudo make install

# Usage

Include "kumpel/kumpel.h" header in your source code and then link your
program against libkumpel.so (-lkumpel flag for gcc).

# API

- int kumpel_start(unsigned int threads): start worker threads.
'threads' indicate number of threads, which should be started, 0 mean auto detect
number of (virtual) CPUs and start same amount of threads.
Returns 0 if at lease 1 thread started, otherwise non 0 value returned.

- typedef void (kumpel_job_f)(void *job_data): job function accept singe argument,
should contain data, needed for the job. No return value are supported, so the error reporting
should be realized through the 'job_data' or by other means.
Job function can be run in different threads, so proper synchronisation means should be used
if needed.

- int kumpel_job_push(kumpel_job_f *func, void *job_data): add new job to the job queue.
It is implemented as an FIFO queue. An available worker thread will take first job from the queue
and run 'func' with 'job_data'.

- int kumpel_wait(): wait until job queue is empty and all worker threads finished their jobs.
When job function generate jobs, those will be also done.

- void kumpel_stop(): all worker threads will be stopped after they finish their jobs.
Afterwards the queue will be freed but job data of unfinished jobs will not be freed and will be lost.

- void kumpel_kill(): kill all threads regardless of that they doing and empty the job queue.
Job data will be not freed and will be lost.

- int kumpel_wait_and_stop(): equivalent to calling kumpel_wait() and then kumpel_stop().
