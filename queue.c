/* Copyright (c) 2020 Andrej Gelenberg <andrej.gelenberg@udo.edu> */

#include <semaphore.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <stdatomic.h>
#include <limits.h>

#include "kumpel/kumpel.h"

struct kumpel_queue_t {
  size_t length;

  size_t push_i;
  size_t pop_i;

  sem_t push_sem;
  sem_t pop_sem;

  void * volatile elements[];
};

kumpel_queue_t *
kumpel_queue_init(
    size_t queue_length) {

  kumpel_queue_t *ret;

  // make sure, no integer overflows will happen by allocating the queue
  if ( ( queue_length > UINT_MAX ) ||
       ( queue_length > ( (SIZE_MAX - sizeof(*ret)) / sizeof(ret->elements[0]) ) ) ) {
    errno = EOVERFLOW;
    goto overflow;
  }

  ret = malloc(sizeof(*ret) + queue_length * sizeof(ret->elements[0]));
  if ( ret == 0 )
    goto end;

  ret->length = queue_length;
  ret->push_i = 0;
  ret->pop_i  = 0;

  // do only thread sharing queue
  if ( sem_init(&(ret->push_sem), 0, queue_length) )
    goto err;

  if ( sem_init(&(ret->pop_sem), 0, 0) )
    goto pop_sem_err;

end:
  return ret;

pop_sem_err:
  sem_destroy(&(ret->push_sem));
err:
  free(ret);
overflow:
  ret = 0;
  goto end;

}

int
kumpel_queue_free(
    kumpel_queue_t *queue) {

  int ret = sem_destroy(&(queue->push_sem));

  if (sem_destroy(&(queue->pop_sem)) )
    ret = -1;

  free(queue);
  
  return ret;

}

int
kumpel_queue_pop(
    kumpel_queue_t  *queue,
              void **value) {

  int ret;

  ret = sem_wait(&(queue->pop_sem));
  if ( ret )
    goto end;

  size_t cur = atomic_fetch_add(&(queue->pop_i), 1);
  *value = queue->elements[ cur % queue->length ];

  ret = sem_post(&(queue->push_sem));

end:
  return ret;

}

int
kumpel_queue_try_pop(
    kumpel_queue_t  *queue,
              void **value) {

  int ret;

  ret = sem_trywait(&(queue->pop_sem));
  if ( ret )
    goto end;

  size_t cur = atomic_fetch_add(&(queue->pop_i), 1);
  *value = queue->elements[ cur % queue->length ];

  ret = sem_post(&(queue->push_sem));

end:
  return ret;

}

int
kumpel_queue_time_pop(
           kumpel_queue_t  *queue,
                     void **value,
    const struct timespec  *timeout) {

  int ret;

  ret = sem_timedwait(&(queue->pop_sem), timeout);
  if ( ret )
    goto end;

  size_t cur = atomic_fetch_add(&(queue->pop_i), 1);
  *value = queue->elements[ cur % queue->length ];

  ret = sem_post(&(queue->push_sem));

end:
  return ret;

}


int
kumpel_queue_push(
    kumpel_queue_t *queue,
              void *value) {

  int ret;

  ret = sem_wait(&(queue->push_sem));
  if ( ret )
    goto end;

  size_t cur = atomic_fetch_add(&(queue->pop_i), 1);
  queue->elements[ cur % queue->length ] = value;

  ret = sem_post(&(queue->pop_sem));

end:
  return ret;

}

int
kumpel_queue_try_push(
    kumpel_queue_t *queue,
              void *value) {

  int ret;

  ret = sem_trywait(&(queue->push_sem));
  if ( ret )
    goto end;

  size_t cur = atomic_fetch_add(&(queue->pop_i), 1);
  queue->elements[ cur % queue->length ] = value;

  ret = sem_post(&(queue->pop_sem));

end:
  return ret;

}

int
kumpel_queue_time_push(
           kumpel_queue_t *queue,
                     void *value,
    const struct timespec *timeout) {

  int ret;

  ret = sem_timedwait(&(queue->push_sem), timeout);
  if ( ret )
    goto end;

  size_t cur = atomic_fetch_add(&(queue->pop_i), 1);
  queue->elements[ cur % queue->length ] = value;

  ret = sem_post(&(queue->pop_sem));

end:
  return ret;

}
