PROJECT_NAME := libkumpel
AUTHOR := Andrej Gelenberg <andrej.gelenberg@udo.edu>
VER_MAJOR := 1
VER_MINOR := 0
 
all: libkumpel.so.$(VER_MAJOR)
CLEAN += libkumpel.so* test

libkumpel.so: libkumpel.o queue.o

CFLAGS += -I.
LDFLAGS += -L.

libkumpel.so: Makefile
libkumpel.so: CFLAGS += -pthread

test: LIBS=-lkumpel
test: test.o libkumpel.so Makefile

.PHONY: run_test
run_test: test
	LD_LIBRARY_PATH=. $$(which time) ./test

$(eval $(call install_so,libkumpel.so))
install: $(foreach i,$(shell find kumpel/ -name '*.h'), $(DESTDIR)/usr/include/$i)
