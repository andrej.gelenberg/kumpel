# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="Kumpel is a c library for organizing an batch processing queue for independent tasks, which is then processed by 1 or multiple worker threads."
HOMEPAGE="https://bitbucket.org/nikel123/kumpel.git"
EGIT_REPO_URI="https://bitbucket.org/nikel123/kumpel.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86 ~amd64"

inherit git-r3
