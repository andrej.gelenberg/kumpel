// Copyright (c) 2014 Andrej Gelenberg <andrej.gelenberg@udo.edu>
#include <stdio.h>
#include <stdlib.h>

#include <kumpel/kumpel.h>

int failed = 0;

static void
job(
    void *data) {

  int n = *((int*)data);

  printf("-> %d\n", n);

  free(data);

}

static void
job1(
    void *data) {

  int i = *((int *)data);
  int e = i + 1000;
  int ret;
  int *di;

  for(; i < e; ++i) {
    di = malloc(sizeof(*di));
    *di = i;
    ret = kumpel_job_push(job, di);
    if ( ret ) {
      fprintf(stderr, "kumpel_job_push() failed\n");
      failed = 1;
      free(di);
      break;
    }
  }

  free(data);

}

int
main(
    int    argc,
    char **argv) {

  int ret = 0;
  int i;
  int *di;

  printf("runing test.\n");

  ret = kumpel_start(0);
  if ( ret ) {
    fprintf(stderr, "kumpel_start() failed.\n");
    goto err;
  }

  for(i = 0; i < 1000000; i += 1000) {
    di = malloc(sizeof(*di));
    *di = i;
    ret = kumpel_job_push(job1, di);
    if ( ret ) {
      fprintf(stderr, "kumpel_job_push() failed.\n");
      free(di);
      goto err_kumpel;
    }
  }

  ret = kumpel_wait_and_stop();
  if ( ret ) {
    fprintf(stderr, "kumpel_wait_and_stop() failed.\n");
    goto err_kumpel;
  }

  if ( failed ) goto err;

  printf("test ok.\n");

end:
  return ret;

err_kumpel:
  kumpel_kill();
err:
  ret = 1;
  printf("test failed.\n");
  goto end;

}
