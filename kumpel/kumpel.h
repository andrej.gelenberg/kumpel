/* Copyright (c) 2014-2020 Andrej Gelenberg <andrej.gelenberg@udo.edu> */

#ifndef _KUMPEL_H
#define _KUMPEL_H

struct kumpel_queue_t;
typedef struct kumpel_queue_t kumpel_queue_t;

kumpel_queue_t *
kumpel_queue_init(
    size_t queue_length);

int
kumpel_queue_free(
    kumpel_queue_t *queue);

int
kumpel_queue_pop(
    kumpel_queue_t  *queue,
              void **value);

int
kumpel_queue_try_pop(
    kumpel_queue_t  *queue,
              void **value);

int
kumpel_queue_time_pop(
           kumpel_queue_t  *queue,
                     void **value,
    const struct timespec  *timeout);

int
kumpel_queue_push(
    kumpel_queue_t *queue,
              void *value);

int
kumpel_queue_try_push(
    kumpel_queue_t *queue,
              void *value);

int
kumpel_queue_time_push(
           kumpel_queue_t *queue,
                     void *value,
    const struct timespec *timeout);

#endif /* _KUMPEL_H */
